# FINAL FANTASY OMNIA OPERA SPREADSHEETS

This project provides spreadsheets related to [Dissidia Final Fantasy Omnia Opera](https://www.reddit.com/r/DissidiaFFOO). Quite a mouthful, I know. It's an excellent mobile game that I've been playing since launch. I personally play it on a Samsung Galaxy Tab 4 with Samsung DeX enabled. It provides a more Windows-like environment so I can multitask while playing.

In fact, that's generally when I work on these spreadsheets -- while grinding, because there is a lot of that. And I mean a lot. The [latest files](https://bitbucket.org/rswfire/gaming-ffoo/downloads/) are always available here. Each release also includes a "sample" file. That's just a copy of my game data as it stood at the time of upload. You can use it for inspiration, a way to visualize how I utilize the spreadsheets myself.

Here are some quick links to the latest available files:

* [FFOO-Character-Progression-v1.0.0.xlsx](https://bitbucket.org/rswfire/gaming-ffoo/downloads/FFOO-Character-Progression-v1.0.0.xlsx)
* [FFOO-Character-Progression-v1.0.0_Sample.xlsx](https://bitbucket.org/rswfire/gaming-ffoo/downloads/FFOO-Character-Progression-v1.0.0_Sample.xlsx)

I hope you enjoy my work and that it makes your gaming experience more efficient. I welcome any feedback you may wish to offer me. I'm "rswfire" all over the web, whether it is [Facebook](https://facebook.com/tswfire), [Twitter](https://twitter.com/rswfire), [Reddit](https://www.reddit.com/user/rswfire), or some other obscure place. Please feel free to reach out to me in whatever means works best for you.

Finally, consider adding me in the game! I'm `658126006`.

Kupow, Kupopo-po!

☆ rswfire

### Some thoughts on the future of this project, originally posted on [Reddit](https://www.reddit.com/r/DissidiaFFOO/comments/alo526/dffoo_character_progression_spreadsheet/efghl57).

Thinking about this, I'm going to automate a lot of the main sheet in the next iteration. You won't need to manually add bullets to every cell.
Instead, on a subtab, I'll list all of the weapons and armor (both sortable and filterable) and you'll just plug in a number next to it indicating the number of limit breaks. From there, it will populate the front page accordingly.

Further, in another column on the subtab, I'll make an optional field where you can plug in the current level of the equipment. If it's at max level, it will change the background for the set on the main sheet automatically. If it isn't, the main sheet will have visual indicators representing this and the subpage will calculate how many orbs are needed for you to max it out.

Finally, further iterations can rely upon this automation to provide greater flexibility and relative complexity. The goal is to give a visual guide to character and team progress in a substantive and meaningful manner. I think there is a great deal of room to improve upon these goals.

In closing, if those features excite you, don't let it stop you from getting started with the first sheet. It's important to punch in all the data so you know where your characters stand. When the next iteration of the spreadsheet comes out, the transition should be smooth and painless because you'll have all of the needed information right in front of you.

The biggest challenge to adopting this spreadsheet is the legwork you must do. You'll have to stop playing for a few hours and methodically examine your characters and your equipment and punch in all the relevant data. That part sucks, especially if you have a big account, but the payoff is worth it, and it's a one-time event.
Afterward, it's just a matter of maintenance. (I'll probably put a guide together to coincide with the next release explaining my process for setting up and maintaining the spreadsheet as efficiently as possible.)